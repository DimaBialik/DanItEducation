// // class Task {
// //     constructor( title = '' ) {
// //      this.title = title;
// //      this.done = false;
// //      Task.count += 1;
// //      console.log('Происходит создание задачи');
// //     }
// //      complete() {
// //      this.done = true;
// //      console.log(`Задача "${this.title}" выполнена!`);
// //     }
// //    };
   
// //    Task.count = 0; 
   
// //    let task = new Task("Выучить JavaScript");
// //    let task2 = new Task("Выучить ES6");
   
   
// //    console.log( task.title );
// // //    console.log( task2.title );
   
// //    console.log( Task.count );
// //    task2.complete();

// const myMap = new Map([[1, 'one'], [2, 'two']]);
// const array = [...myMap] // [ [ 1, 'one' ], [ 2, 'two' ] ]
// //Так же, как и с Set, мы можем конвертировать массив обратно в Map, передав его в new Map

// console.log(myMap);
// console.log(array);
// //new Map(array); // Map { 1 => 'one', 2 => 'two' }

let promise = new Promise(function(resolve, reject) {
    setTimeout(() => resolve("done!"), 1000);
  });
  
  // resolve запустит первую функцию, переданную в .then
  promise.then(
    result => alert(result), // выведет "done!" через одну секунду
    error => alert(error) // не будет запущена
  );