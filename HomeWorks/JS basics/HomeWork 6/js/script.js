// метод 1
function filterBy(array, type) {
    return array.reduce((acc, el) => {
        if (typeof el != type) {
            acc.push(el);
        }
        return acc;
    }, []);
}

console.log(filterBy(['hello', 'world', 23, '23', null, false], 'string'));
console.log(filterBy(['hello', 'world', 23, '23', null, false], 'number'));

// метод 2

const filterBy1 = (arr, type) => arr.filter(item => typeof item !== type);

console.log(filterBy1(['hello', 'world', 23, '23', null, false], 'boolean'));
