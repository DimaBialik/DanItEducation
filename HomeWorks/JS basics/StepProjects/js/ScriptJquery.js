$(document).ready(function () {
    $('.hide-authors,.hide-authors-mini,.loader,.show,.items-image,.hide-best-image').hide();

    $('a').on('click', function (e) {
        e.preventDefault();
    });

    $('.li-amazing').on('click', showOurAmazingWork);
    $('.load-more-amazing').on('click', loadMoreAmazing);
    $('.mini-circle-photo').on('click', showPeople);
    $('.left').on('click', moveLeft);
    $('.right').on('click', moveRight);

    function showOurServices() {
        $('.services-menu-content').not(':first').hide();
        $('.services-menu .li-list')
            .click(function () {
                $('.services-menu .li-list')
                    .removeClass('active')
                    .eq($(this).index())
                    .addClass('active');
                $('.services-menu-content').hide('700').eq($(this).index()).fadeIn('700');
            })
            .eq(0)
            .addClass('active');
    }
    showOurServices();

    function loadMoreAmazingStart() {
        let j = 12;
        for (let i = 0; i < j; i++) {
            $('.items-image').eq(i).show();
        }
    }
    loadMoreAmazingStart();
    function showOurAmazingWork() {
        $('.load-more-amazing').show();
        $('.amazing-items').removeClass('margin-bottom');

        let liListAmazing = $('.li-amazing');
        for (let item of liListAmazing) {
            if ($(item).hasClass('active-amazing')) {
                $(item).removeClass('active-amazing');
            }
        }
        $(this).addClass('active-amazing');

        let dataItems = $(this).data('amazingItems');
        let itemsImage = Array.from($('.items-image'));
        let showItems = 12;
        let dataItemsImage = itemsImage.filter(value => {
            return value.getAttribute('data-amazing-items') === dataItems;
        });
        $(itemsImage).hide();
        let hideButtonAddClass = () => {
            $('.load-more-amazing').hide();
            $('.amazing-items').addClass('margin-bottom');
        };

        if (dataItemsImage.length > showItems) {
            for (let i = 0; i < showItems; i++) {
                $(dataItemsImage).eq(i).fadeIn(700);
            }
            $('.load-more-amazing').show();
        } else if (dataItems === 'all') {
            $(itemsImage).fadeIn(700);
            hideButtonAddClass();
        } else {
            $(dataItemsImage).fadeIn(700);
            hideButtonAddClass();
        }
    }

    function loadMoreAmazing() {
        let lastVisIndex = $('.items-image:visible:last').index();
        let allItemsImage = $('.items-image:last').index();
        let hiddenElem = $('.items-image:hidden');
        let dataAmazingItems = $('.active-amazing').data('amazingItems');
        console.log(dataAmazingItems);
        let allElemDataItems = Array.from(
            $('.items-image').filter(function () {
                return $(this).data('amazingItems') === dataAmazingItems;
            })
        );
        let showItems = 12;

        $('.loader-image').show();
        $('.load-more-amazing').hide();

        setTimeout(function () {
            $('.loader-image').hide();
            $('.load-more-amazing').show();

            if (dataAmazingItems === 'all') {
                for (let i = 0; i < showItems; i++) {
                    $(hiddenElem).eq(i).fadeIn(700);
                }
            } else {
                for (let i = showItems; i <= allElemDataItems.length - 1; i++) {
                    $(allElemDataItems).eq(i).fadeIn(700);
                }
                $('.load-more-amazing').hide();
                $('.amazing-items').addClass('margin-bottom');
            }
            lastVisIndex = $('.items-image:visible:last').index();
            if (allItemsImage === lastVisIndex) {
                $('.load-more-amazing').hide();
                $('.amazing-items').addClass('margin-bottom');
            }
        }, 1000);
    }
    function showPeople() {
        let people = $(this).data('people');
        let bigPeopleBlock = $('.authors-main-block');
        $(bigPeopleBlock).hide();

        for (let item of bigPeopleBlock) {
            if ($(item).data('people') === people) {
                $(item).fadeIn(700);
            }
        }

        $('.mini-circle-photo').removeClass('mini-circle-border');
        $(this).addClass('mini-circle-border');
    }
    function moveLeft() {
        let currentImage = $('.mini-circle-photo.mini-circle-border');
        let currentImageIndex = currentImage.index();
        let prevImageIndex = currentImageIndex - 1;
        let prevImage = $('.mini-circle-photo').eq(prevImageIndex);
        let bigPeopleBlock = $('.authors-main-block');
        $(bigPeopleBlock).hide();

        if (currentImageIndex === $('.mini-circle-photo:visible:first').index()) {
            $('.mini-circle-photo').eq(prevImageIndex).show();
            $('.mini-circle-photo')
                .eq(currentImageIndex + 3)
                .hide();
        }
        if (currentImageIndex === $('.mini-circle-photo:first').index()) {
            $('.mini-circle-photo:hidden').show();
            prevImage = $('.mini-circle-photo:last');
            currentImage.hide();
        }

        currentImage.removeClass('mini-circle-border');
        prevImage.addClass('mini-circle-border');
        for (let item of bigPeopleBlock) {
            if (
                $(item).data('people') === $('.mini-circle-photo.mini-circle-border').data('people')
            ) {
                $(item).fadeIn(700);
            }
        }
    }

    function moveRight() {
        let currentImage = $('.mini-circle-photo.mini-circle-border');
        let currentImageIndex = currentImage.index();
        let nextImageIndex = currentImageIndex + 1;
        let nextImage = $('.mini-circle-photo').eq(nextImageIndex);
        let bigPeopleBlock = $('.authors-main-block');

        $(bigPeopleBlock).hide();
        if (currentImageIndex === $('.mini-circle-photo:visible:last').index()) {
            $('.mini-circle-photo').eq(nextImageIndex).show();
            $('.mini-circle-photo')
                .eq(currentImageIndex - 3)
                .hide();
        }
        if (currentImageIndex === $('.mini-circle-photo:last').index()) {
            $('.mini-circle-photo:hidden').show();
            nextImage = $('.mini-circle-photo').eq(0);
            currentImage.hide();
        }

        currentImage.removeClass('mini-circle-border');
        nextImage.addClass('mini-circle-border');
        for (let item of bigPeopleBlock) {
            if (
                $(item).data('people') === $('.mini-circle-photo.mini-circle-border').data('people')
            ) {
                $(item).fadeIn(700);
            }
        }
    }
});
