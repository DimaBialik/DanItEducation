$(document).ready(function () {
    let tab = $('.tabs .tabs-items .tabs-item');
    let navTab = $('.tabs .tabs-nav a');
    tab.hide().filter(':first').show();

    navTab.click(function () {
        tab.hide().filter(this.hash).show();

        navTab.removeClass('active');
        $(this).addClass('active');
        return false;
    });

    $('.next').click(function () {
        let currentTab = $('.tab.activeClient');
        let newTab = currentTab.next();
        if (newTab.length === 0) {
            newTab = $('.tab').first();
        }
        currentTab.removeClass('activeClient');
        newTab.addClass('activeClient');
        newTab.trigger('click');
    });
    $('.prev').click(function () {
        let currentTab = $('.tab.activeClient');
        let newTab = currentTab.prev();
        if (newTab.length === 0) {
            newTab = $('.tab').last();
        }
        currentTab.removeClass('activeClient');
        newTab.addClass('activeClient');
        newTab.trigger('click');
    });
});

$('.services-menu-content').not(':first').hide();
$('.services-menu .li-list')
    .click(function () {
        $('.services-menu .li-list').removeClass('active').eq($(this).index()).addClass('active');
        $('.services-menu-content').hide().eq($(this).index()).fadeIn();
    })
    .eq(0)
    .addClass('active');

$('.authors-main-block').not(':first').hide();
$('.karusel .mini-circle-photo')
    .click(function () {
        $('.karusel .mini-circle-photo')
            .removeClass('active')
            .eq($(this).index())
            .addClass('active');
        $('.authors-main-block').hide().eq($(this).index()).fadeIn();
    })
    .eq(0)
    .addClass('active');
function moveLeft() {
    let currentImage = $('.mini-circle-photo.mini-circle-border');
    let currentImageIndex = currentImage.index();
    let prevImageIndex = currentImageIndex - 1;
    let prevImage = $('.mini-circle-photo').eq(prevImageIndex);
    let bigPeopleBlock = $('.authors-main-block');
    $(bigPeopleBlock).hide();

    if (currentImageIndex === $('.mini-circle-photo:visible:first').index()) {
        $('.mini-circle-photo').eq(prevImageIndex).show();
        $('.mini-circle-photo')
            .eq(currentImageIndex + 3)
            .hide();
    }
    if (currentImageIndex === $('.mini-circle-photo:first').index()) {
        $('.mini-circle-photo:hidden').show();
        prevImage = $('.mini-circle-photo:last');
        currentImage.hide();
    }

    currentImage.removeClass('mini-circle-border');
    prevImage.addClass('mini-circle-border');
    for (let item of bigPeopleBlock) {
        if ($(item).data('people') === $('.mini-circle-photo.mini-circle-border').data('people')) {
            $(item).fadeIn(700);
        }
    }
}

function moveRight() {
    let currentImage = $('.mini-circle-photo.mini-circle-border');
    let currentImageIndex = currentImage.index();
    let nextImageIndex = currentImageIndex + 1;
    let nextImage = $('.mini-circle-photo').eq(nextImageIndex);
    let bigPeopleBlock = $('.authors-main-block');

    $(bigPeopleBlock).hide();
    if (currentImageIndex === $('.mini-circle-photo:visible:last').index()) {
        $('.mini-circle-photo').eq(nextImageIndex).show();
        $('.mini-circle-photo')
            .eq(currentImageIndex - 3)
            .hide();
    }
    if (currentImageIndex === $('.mini-circle-photo:last').index()) {
        $('.mini-circle-photo:hidden').show();
        nextImage = $('.mini-circle-photo').eq(0);
        currentImage.hide();
    }

    currentImage.removeClass('mini-circle-border');
    nextImage.addClass('mini-circle-border');
    for (let item of bigPeopleBlock) {
        if ($(item).data('people') === $('.mini-circle-photo.mini-circle-border').data('people')) {
            $(item).fadeIn(700);
        }
    }
}

$('.left').on('click', moveLeft);
$('.right').on('click', moveRight);
