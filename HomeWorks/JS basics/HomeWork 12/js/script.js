let img = document.querySelectorAll('.image-to-show');
let slideNum = 0;
let interval = setInterval(changeInterval, 3000);
function changeInterval() {
    for (let i of img) {
        i.style.display = 'none';
    }
    img[slideNum++].style.display = 'block';
    slideNum === img.length ? (slideNum = 0) : true;
}
function startStop() {
    let start = document.querySelector('.start');
    start.addEventListener('click', () => {
        stop.disabled = false;
        start.disabled = true;

        console.log('start');
        interval = setInterval(changeInterval, 3000);
    });
    let stop = document.querySelector('.stop');
    stop.addEventListener('click', () => {
        start.disabled = false;
        stop.disabled = true;

        console.log('stop');
        clearInterval(interval);
    });
}
changeInterval();
startStop();
