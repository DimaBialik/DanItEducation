let input = document.createElement('input');
let spanInput = document.createElement('span');

document.body.append(spanInput);
spanInput.innerText = 'Price: ';
spanInput.append(input);
document.body.style.cssText = `
padding-top:20px`;
let styleSpanInput = (spanInput.style.cssText = `
border: 2px solid black;
padding: 10px;
border-radius: 7px;
margin:20px;
`);

input.onfocus = () => {
    spanInput.style.border = '2px solid green';
};

input.onblur = () => {
    spanInput.style.border = '2px solid black';

    if (input.onblur) {
        if (isNaN(input.value) || input.value < 0) {
            let spanError = document.createElement('span');

            spanInput.after(spanError);

            spanError.innerText = `Please enter correct price`;
            spanError.style.cssText = `
            border: 1px solid black;
            padding: 5px;
            border-radius: 7px;
            margin-left:10px;`;
            spanInput.style.background = 'red';
        } else if (Number(input.value)) {
            let spanPrice = document.createElement('span');
            let btn = document.createElement('button');

            btn.innerText = 'X';
            spanPrice.innerText = `Текущая цена: ${input.value}`;
            spanInput.before(spanPrice);
            spanPrice.append(btn);

            btn.onclick = () => {
                spanPrice.remove();
            };
            spanInput.style.background = 'white';
            spanPrice.style.cssText = `
            border: 1px solid black;
            padding: 5px;
            border-radius: 7px;
            margin-left:10px;
            `;
            btn.style.cssText = `
            background:white;
            margin-left:20px;
            border-radius: 5px;
            border: 1px solid black;`;
        }
    }
};
// я не понял как сделать что бы spanError появился только 1 раз
