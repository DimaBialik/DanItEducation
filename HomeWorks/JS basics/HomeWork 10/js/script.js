function checkInput() {
    let input = document.querySelectorAll('.password-input');
    let icon = document.querySelectorAll('.input-wrapper>i:last-child');
    let btn = document.querySelector('.btn');

    input.forEach(el => {
        el.addEventListener('click', showHidePassword);
    });

    function showHidePassword() {
        if (this.getAttribute('type') === 'password') {
            this.setAttribute('type', 'text');
        } else {
            this.setAttribute('type', 'password');
        }
        hideIcon();
    }
    function hideIcon() {
        icon.forEach(el => {
            el.classList.toggle('hide');
        });
    }
    btn.addEventListener('click', checkValue);

    function checkValue() {
        let inputFirst = document.querySelector('.password-input.first');
        let inputSecond = document.querySelector('.password-input.second');
        errorSpanRemove();
        if (
            inputFirst.value !== inputSecond.value ||
            inputFirst.value === '' ||
            inputSecond.value === ''
        ) {
            errorMessage();
        } else {
            alert('You are welcome');
            inputFirst.value = '';
            inputSecond.value = '';
        }
    }
    function errorMessage() {
        let span = document.createElement('span');
        span.classList.add('errorSpan');
        span.innerText = 'Нужно ввести одинаковые значения';
        btn.before(span);
    }
    function errorSpanRemove() {
        let findErrorSpan = document.querySelector('.errorSpan');
        if (findErrorSpan) {
            findErrorSpan.remove();
        }
    }
}
checkInput();
