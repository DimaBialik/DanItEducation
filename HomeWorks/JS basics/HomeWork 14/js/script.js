$(document).ready(function () {
    //#1
    $('.menu-link').click(function () {
        //#2
        let linkScroll = $(this).attr('href'); // #3
        $('html, body').animate(
            //#4
            {
                scrollTop: $(linkScroll).offset().top // #5
            },
            'slow' // #6
        );
    });
    $(document).on('scroll', function () {
        //#7
        if ($(this).scrollTop() > 100) {
            //#8
            if ($('#upbutton').is(':hidden')) {
                $('#upbutton').css({ opacity: 1 }).fadeIn('slow');
            }
        } else {
            $('#upbutton').stop(true, false).fadeOut('slow');
        }
    });
    $('#upbutton').on('click', function () {
        // #9
        $('html, body').stop().animate({ scrollTop: 0 }, 300);
    });
    $('.btn-slideTogle').click(function () {
        // #10
        $('.hot-news-images').toggle('slow');
    });
});

// #1 выполнения функции после того как страница загрузилась
// №2 нашел все сылки которые будут выполнять действие
// #3 создал переменую и даю ей значение атрибута href
// #4 в нутри html, body будет анимация
// #5 скролит до нужного якоря
// #6 время в течении которого произойдет анимация
// #7 выполнения функции после того как произошол scroll
// #8 если скрол больше 100px появляеться кнопка up
// #9 при клике скрол вверх
// #10 при клике тоглить часть сайта
