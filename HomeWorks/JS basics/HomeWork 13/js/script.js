let darkTheme = document.querySelector('.changeTheme');

let link = document.querySelector('.theme');
console.log(link);

darkTheme.addEventListener('click', changeTheme);

function changeTheme() {
    localStorage['data-theme'] === 'css/light.css'
        ? localStorage.setItem('data-theme', 'css/dark.css')
        : localStorage.setItem('data-theme', 'css/light.css');
    let localTheme = localStorage.getItem('data-theme');
    link.setAttribute('href', localTheme);
}
let localValue = localStorage.getItem('data-theme');
link.setAttribute('href', localValue);
