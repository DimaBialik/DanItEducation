let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];

let parent = document.createElement('ul');
document.body.append(parent);

function addList(arrEl, parentEl) {
    arrEl.map(el => {
        let li = document.createElement('li');
        li.innerHTML = el;
        parentEl.append(li);
    });
}
addList(arr, parent);
