let tab = function () {
    let tabs = document.querySelectorAll('.tabs-title');
    let tabsContent = document.querySelectorAll('.tab');
    let tabTarget = null;

    tabs.forEach(item => {
        item.addEventListener('click', getTabs);
    });

    function getTabs() {
        tabs.forEach(item => {
            item.classList.remove('active');
        });
        this.classList.add('active');
        tabTarget = this.getAttribute('data-tab-name');
        getTabsCont(tabTarget);
    }

    function getTabsCont(tabName) {
        tabsContent.forEach(item => {
            if (item.classList.contains(tabName)) {
                item.classList.add('active');
            } else {
                item.classList.remove('active');
            }
        });
    }
};

tab();
