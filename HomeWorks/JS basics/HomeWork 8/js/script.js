let input = document.createElement('input');
let spanInput = document.createElement('span');
spanInput.innerText = 'price: $ ';
document.body.append(spanInput);
spanInput.append(input);

input.addEventListener('focus', createInputFocus);
input.addEventListener('blur', createInputBlur);

function createInputFocus() {
    input.style.cssText = `
    outline: none;
    border: none;
    border: 2px solid  green;
    `;
    createRemoveError();
}

function createInputBlur() {
    input.style.cssText = `
    outline: none;
    border: none;
    border: 2px solid  black;
    `;
    if (isNaN(input.value) || input.value < 0) {
        const spanError = createSpanError();
        spanInput.after(spanError);
        input.value = '';

        input.style.cssText = `
        outline: none;
        border: none;
        border: 2px solid  red;
    `;
    } else if (Number(input.value)) {
        const spanPrice = createSpanPrice();
        spanInput.before(spanPrice);
        input.value = '';
        const btn = createRemoveBtn();
        spanPrice.append(btn);
        input.style.cssText = `
        outline: none;
        border: none;
        background-color: green;
    `;
    }
}
function createSpanError() {
    let spanError = document.createElement('span');
    spanError.innerText = `Please enter correct price`;

    return spanError;
}
function createSpanPrice() {
    let spanPrice = document.createElement('span');
    spanPrice.innerText = `Текущая цена: ${input.value} `;

    return spanPrice;
}
function createRemoveError() {
    input.parentNode.nextSibling.remove();
}
function createRemoveBtn() {
    let btn = document.createElement('button');
    btn.innerText = 'x';
    btn.addEventListener('click', e => {
        e.target.closest('span').remove();
    });
    return btn;
}
