
const createNewUser = ( ) => {
    const newUser = {
        firstName:  prompt('Enter u firstName'),
        lastName:  prompt('Enter u lastName'),
        birthday:  prompt('Enter u birthday', 'dd.mm.yyyy'),

        getLogin: function () {
            return this.firstName.slice(0, 1).toLowerCase() + this.lastName.toLowerCase();
        },
        getAge : function () {
            const today = new Date();
            const userBirthday = Date.parse(`${this.birthday.slice(6)}-${this.birthday.slice(3, 5)}-${this.birthday.slice(0, 2)}`);
            const age = ((today - userBirthday) / (1000 * 60 * 60 * 24 * 30 * 12)).toFixed(0);
            if (age < today) {
              return `Вам ${age - 1} лет`;
            } else {
              return `Вам ${age} лет`;
            }
                },
        getPassword: function() { 
          return this.firstName.slice(0, 1).toUpperCase()+this.lastName.toLowerCase()+this.birthday.slice(6);
        }
          
    };
    return newUser;
};

const user = createNewUser();

console.log(user.firstName);
console.log(user.lastName);

console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());

