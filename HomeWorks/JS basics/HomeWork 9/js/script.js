let tab = function () {
    let tabsCont = document.querySelector('.tabs');
    let tabsContent = document.querySelectorAll('.tab');
    let tabTarget = null;

    tabsCont.addEventListener('click', getTabs);

    function getTabs(e) {
        let activeTab = document.querySelector('.active');
        activeTab ? activeTab.classList.remove('active') : true;
        e.target.classList.add('active');
        tabTarget = e.target.dataset.tab;
        getTabsCont(tabTarget);
    }

    function getTabsCont(tabName) {
        tabsContent.forEach(item => {
            item.classList.contains(tabName)
                ? item.classList.add('active')
                : item.classList.remove('active');
        });
    }
};

tab();
