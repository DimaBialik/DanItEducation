let arr = [
    'Kharkiv',
    'Kiev',
    ['Borispol', 'Irpin'],
    'Odessa',
    'Lviv',
    'Dnieper'
];

let parent = document.createElement('ul');
document.body.insertAdjacentElement('afterbegin', parent);

function addList(arrEl, parentEl) {
    arrEl.map(el => {
        let li = document.createElement('li');
        li.innerHTML = el;
        parentEl.insertAdjacentElement('afterbegin', li);
    });
}
addList(arr, parent);
