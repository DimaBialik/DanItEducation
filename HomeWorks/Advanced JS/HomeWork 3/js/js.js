const tack1 = function () {
    const clients1 = ['Гилберт', 'Сальваторе', 'Пирс', 'Соммерс', 'Форбс', 'Донован', 'Беннет'];
    const clients2 = ['Пирс', 'Зальцман', 'Сальваторе', 'Майклсон'];

    console.log('task1 ', [...new Set([...clients1, ...clients2])]);
};
tack1();

const tack2 = function () {
    const characters = [
        {
            name: 'Елена',
            lastName: 'Гилберт',
            age: 17,
            gender: 'woman',
            status: 'human'
        },
        {
            name: 'Кэролайн',
            lastName: 'Форбс',
            age: 17,
            gender: 'woman',
             status: 'human'
        },
        {
            name: 'Аларик',
            lastName: 'Зальцман',
            age: 31,
            gender: 'man',
            status: 'human'
        },
        {
            name: 'Дэймон',
            lastName: 'Сальваторе',
            age: 156,
            gender: 'man',
            status: 'vampire'
        },
        {
            name: 'Ребекка',
            lastName: 'Майклсон',
            age: 1089,
            gender: 'woman',
            status: 'vempire'
        },
        {
            name: 'Клаус',
            lastName: 'Майклсон',
            age: 1093,
            gender: 'man',
            status: 'vampire'
        }
    ];
    const charactersShortInfo = characters.map(function (item) {
        ({ name, lastName, age } = item);
        return { name, lastName, age };
    });
    console.log('task2 ', charactersShortInfo);
};
tack2();

const tack3 = function () {
    const user1 = {
        name: 'John',
        years: 30
    };
    const { name, years: age, isAdmin = false } = user1;
    console.log('task3 ', `name: ${name}; age: ${age}; isAdmin: ${isAdmin}`);
};
tack3();

const tack4 = function () {
    const satoshi2020 = {
        name: 'Nick',
        surname: 'Sabo',
        age: 51,
        country: 'Japan',
        birth: '1979-08-21',
        location: {
            lat: 38.869422,
            lng: 139.876632
        }
    };

    const satoshi2019 = {
        name: 'Dorian',
        surname: 'Nakamoto',
        age: 44,
        hidden: true,
        country: 'USA',
        wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
        browser: 'Chrome'
    };

    const satoshi2018 = {
        name: 'Satoshi',
        surname: 'Nakamoto',
        technology: 'Bitcoin',
        country: 'Japan',
        browser: 'Tor',
        birth: '1975-04-05'
    };

    const fullProfile = { ...satoshi2018, ...satoshi2019, ...satoshi2020 };
    console.log('task4 ', fullProfile);
};
tack4();

const tack5 = function () {
    const books = [
        {
            name: 'Harry Potter',
            author: 'J.K. Rowling'
        },
        {
            name: 'Lord of the rings',
            author: 'J.R.R. Tolkien'
        },
        {
            name: 'The witcher',
            author: 'Andrzej Sapkowski'
        }
    ];

    const bookToAdd = {
        name: 'The Alchemist',
        author: 'Paulo Coelho'
    };

    const newBooks = [...books, bookToAdd];
    console.log('task5 ', newBooks);
};
tack5();

const tack6 = function () {
    const employee = {
        name: 'Vitalii',
        surname: 'Klichko'
    };

    const { name, surname, age = 50, salary = 300 } = employee;
    console.log('task6 ', { name, surname, age, salary });
};
tack6();

const tack7 = function () {
    const array = ['value', () => 'showValue'];
    [value, showValue] = array;

    alert(`task7 - value`);
    alert(`task7 - showValue()`);
};
tack7();
