function homeWork4() {
    const ul = document.querySelector('ul');

    fetch('https://ajax.test-danit.com/api/swapi/films')
        .then(response => response.json())
        .then(data => {
            for (let element of data) {
                addFilmName(element, ul);
                element.characters.forEach(item => {
                    getActors(item, element);
                });
            }
        });
    function addFilmName({ name, episodeId, openingCrawl }, parentElement) {
        const li = document.createElement('li');
        li.dataset.id = episodeId;
        li.innerHTML = `<h2>Епізод: ${episodeId}</h2><h4>Назва: ${name}</h4><ul>Персонажи: </ul><p>Коротко про фільм:  ${openingCrawl}</p>`;
        parentElement.append(li);
    }
    function getActors(item, i) {
        fetch(item)
            .then(response => response.json())
            .then(data => {
                addActors(data, i.episodeId);
            });
    }
    function addActors({ name }, id) {
        let rootElement = document.querySelector(`[data-id="${id}"]`);
        let ul = rootElement.querySelector('ul');
        let li = document.createElement('li');
        li.textContent = name;
        ul.append(li);
    }
}
homeWork4();
