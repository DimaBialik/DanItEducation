const root = document.querySelector('#root');
const h1 = document.querySelector('h1');

const table = document.createElement('table');
const btn = document.createElement('button');
const p = document.createElement('p');

root.append(table);
table.className = 'wrap';
btn.innerHTML = 'start';
h1.after(btn);
btn.after(p);

let arrCell = [];
let arrNumber = [];

class Field {
    constructor() {
        this.arrCell = arrCell;
        this.arrNumber = arrNumber;
    }

    buildField(number) {
        for (let i = 0; i < number; i++) {
            const cell = document.createElement('div');
            cell.className = 'cell';
            cell.style.backgroundImage = '';
            this.arrCell.push(cell);
            this.arrNumber.push(i);
            table.appendChild(cell);
        }
    }
    startBtn() {
        btn.addEventListener('click', () => {
            const newGame = new Game();

            let count = 3;
            function anim() {
                if (count >= 0) {
                    p.innerText = count;
                    count--;
                    setTimeout(anim, 700);
                } else {
                    p.style.color = 'white';
                    function time() {
                        newGame.startGame();
                    }
                    setTimeout(time, 700);
                }
            }
            anim();
        });
    }
}

class Cell {
    constructor() {
        this.arrNumber = arrNumber;
    }

    activeCell() {
        return this.arrNumber.splice(Math.floor(Math.random() * this.arrNumber.length), 1);
    }
}

class Game {
    constructor(level = 'easy') {
        this.level = this.choseLevel();

        if (this.level === 'easy') {
            this.timeInterval = 1500;
        } else if (this.level === 'medium') {
            this.timeInterval = 1000;
        } else if (this.level === 'hard') {
            this.timeInterval = 500;
        }
        this.pointsUser = 0;
        this.pointsPc = 0;
        this.processTimer = null;
    }

    choseLevel() {
        let enterLevel;
        while (enterLevel !== 1 && enterLevel !== 2 && enterLevel !== 3) {
            enterLevel = +prompt(
                'Let begin new game! Select the difficulty level: easy - 1, medium  - 2, hard - 3 '
            );
        }

        if (enterLevel === 1) return 'easy';
        if (enterLevel === 2) return 'medium';
        if (enterLevel === 3) return 'hard';
    }

    playerAction() {
        table.addEventListener('click', function (e) {
            if (e.target.style.background === 'green') {
                const cell = e.target;

                cell.style.background = 'blue';
                clearTimeout(this.processTimer);
            }
        });
    }

    whoIsWinner() {
        if (this.pointsUser > this.pointsPc) {
            alert(`Congratulations! You won with the score ${this.pointsUser} : ${this.pointsPc}`);
        }
        if (this.pointsUser < this.pointsPc) {
            alert(`You lost with the score ${this.pointsUser} : ${this.pointsPc}`);
        }
        alert('Good luck in new game');
        location.reload();
    }

    startGame() {
        const goal = cell.activeCell();
        if (this.pointsUser >= arrCell.length / 2 || this.pointsPc >= arrCell.length / 2) {
            return this.whoIsWinner();
        }

        if (arrCell[goal].style.background === '') {
            arrCell[goal].style.background = 'green';
            arrNumber.slice(arrNumber.indexOf(goal), 1);
            this.playerAction();
            this.processTimer = setTimeout(() => {
                if (arrCell[goal].style.background !== 'blue') {
                    arrCell[goal].style.background = 'red';
                    this.pointsPc++;
                } else if (arrCell[goal].style.background === 'green') {
                    this.pointsUser++;
                }

                this.startGame();
            }, this.timeInterval);
        }
    }
}

const newField = new Field();
newField.buildField(100);
newField.startBtn();
const cell = new Cell();
