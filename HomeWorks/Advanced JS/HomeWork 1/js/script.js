class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._age;
    }
    set name(value) {
        this._name = value;
    }

    get age() {
        return this._age;
    }
    set age(value) {
        this._age = value;
    }
    get salary() {
        return this._salary;
    }
    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }
    set lang(value) {
        this._lang = value;
    }
    get salary() {
        return this._salary * 3;
    }
}
const programmer1 = new Programmer('Artem', 18, 1000, ['js', 'c#']);
const programmer2 = new Programmer('Andrew', 19, 2000, ['c++', 'c#']);
const programmer3 = new Programmer('Margarita', 23, 8000, ['c#', 'rust']);

console.log(programmer1);
console.log(programmer2);
console.log(programmer3);
