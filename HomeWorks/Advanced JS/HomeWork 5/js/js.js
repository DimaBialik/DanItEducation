const users = 'https://ajax.test-danit.com/api/json/users';
const posts = 'https://ajax.test-danit.com/api/json/posts';
//const deletePost = '"http://ajax.test-danit.com/api/json/users/1"';

class Cards {
    constructor(users, posts) {
        this.users = users;
        this.posts = posts;
        //this.deletePost = deletePost;
    }
    newComents() {
        fetch(this.posts, {
            method: 'GET'
        })
            .then(response => {
                return response.json();
            })
            .then(data => {
                for (let item of data) {
                    this.comentUser(item);
                }
            });
    }
    comentUser(item) {
        let posts = document.querySelector('#root');
        let wrapper = document.createElement('div');
        wrapper.classList = 'wrapper';

        posts.append(wrapper);
        let div = document.createElement('div');
        let btn = document.createElement('button');
        btn.innerHTML = 'DELETE POST';
        // btn.dataset.id = item.id;

        div.dataset.user = item.userId;
        wrapper.append(div);
        div.after(btn);
        div.innerHTML = `<p>post # ${item.id}</p>
        <p><span>Title: </span>   ${item.title}</p>
        <p><span>Article: </span>  ${item.body}</p>`;
        btn.addEventListener('click', () => {
            fetch(`https://ajax.test-danit.com/api/json/posts/${item.id}`, {
                method: 'DELETE'
            }).then(response => {
                if (response.ok) {
                    wrapper.style.display = 'none';
                    return response;
                }
            });
        });
    }

    newPost() {
        fetch(this.users, {
            method: 'GET'
        })
            .then(response => {
                return response.json();
            })
            .then(data => {
                for (let item of data) {
                    this.postUser(item);
                }
            });
    }
    postUser(item) {
        let rootElementq = document.querySelectorAll(`[data-user="${item.id}"]`);
        for (let el of rootElementq) {
            let div = document.createElement('div');

            el.after(div);

            div.innerHTML = ` <p class="user-name">
            <span>User-name: </span>  ${item.name}</p>
              <p class="user-email"><span>User-email:  </span> ${item.email}</p>
                                                 `;
            //          <div>
            //          <button class="edit">EDIT POST</button>
            //         <button data-delete class="btnDelete">DELETE POST</button>
            //   </div>
        }

        // let btnDelete = document.querySelector('.btnDelete');
        // btnDelete.addEventListener('click', e => {
        //     console.log(e.target);
        // });
    }
    // newPost() {
    //     fetch('http://ajax.test-danit.com/api/json/users/1', {
    //         method: 'DELETE'
    //     }).then(response => console.log(response.json));
    // }
}
const cards = new Cards(users, posts);

cards.newComents();
cards.newPost();

// function newPost1() {
//     fetch('http://ajax.test-danit.com/api/json/users/1', {
//         method: 'DELETE'
//     }).then(response => console.log(response.json));
// }
