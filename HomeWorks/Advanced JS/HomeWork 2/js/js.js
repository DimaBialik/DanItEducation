const books = [
    {
        author: 'Скотт Бэккер',
        name: 'Тьма, что приходит прежде',
        price: 70
    },
    {
        author: 'Скотт Бэккер',
        name: 'Воин-пророк'
    },
    {
        name: 'Тысячекратная мысль',
        price: 70
    },
    {
        author: 'Скотт Бэккер',
        name: 'Нечестивый Консульт',
        price: 70
    },
    {
        author: 'Дарья Донцова',
        name: 'Детектив на диете',
        price: 40
    },
    {
        author: 'Дарья Донцова',
        name: 'Дед Снегур и Морозочка'
    }
];

books.forEach(item => {
    try {
        checkList(item);
    } catch (error) {
        console.log('Ошибка входных данных.' + error.message);
    }
});
function checkList(item) {
    if (!item.name) {
        throw new SyntaxError(` Свойства name нет в книге автора ${item.author}`);
    }
    if (!item.author) {
        throw new SyntaxError(` Свойства author нет в книге с названием ${item.name}`);
    }
    if (!item.price) {
        throw new SyntaxError(` Свойства price нет в книге с названием ${item.name}`);
    }
}

const listEl = document.createElement('ul');
const root = document.querySelector('#root');

root.append(listEl);
makeList(books);

function makeList(obj) {
    obj.forEach(item => {
        const listChild = document.createElement('ul');
        const author = document.createElement('li');
        const name = document.createElement('li');
        const price = document.createElement('li');

        listChild.innerHTML = `Книга `;
        author.innerHTML = `Имя автора: ${item.author}`;
        name.innerHTML = `Название: ${item.name}`;
        price.innerHTML = `Цена: ${item.price}`;

        if (item.author !== undefined && item.name !== undefined && item.price !== undefined) {
            listEl.append(listChild);
            listChild.append(author, name, price);
        }
    });
}
