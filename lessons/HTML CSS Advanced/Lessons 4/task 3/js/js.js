const menuBtn = document.querySelector('.menu-btn');
const menuBurger = document.querySelector('.menu__burger');
const body = document.querySelector('body');
let menuOpen = false;
menuBtn.addEventListener('click', () => {
    if (!menuOpen) {
        menuBtn.classList.add('open');
        menuBurger.classList.add('open');
        menuOpen = true;
    } else {
        menuBtn.classList.remove('open');
        menuBurger.classList.remove('open');
        menuOpen = false;
    }
});

body.onresize = e => {
    if (e.target.innerWidth >= 500) {
        menuBtn.classList.remove('open');
        menuBurger.classList.remove('open');
        menuOpen = false;
    }
};
