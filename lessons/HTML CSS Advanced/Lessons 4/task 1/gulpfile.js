const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));

function buildStyles() {
    return gulp
        .src('./scss/index.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./'));
}

function watchStyles() {
    gulp.watch('./scss/*.scss', gulp.series(buildStyles));
}

gulp.task('run', watchStyles);
