const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const del = require('del');

gulp.task('moveHTML', function () {
	return gulp
		.src('src/*.html')
		.pipe(htmlmin({ collapseWhitespace: true }))
		.pipe(gulp.dest('dest'));
});

gulp.task('moveCSS', function () {
	return gulp
		.src('src/styles/*.css')
		.pipe(concat('styles.css'))
		.pipe(gulp.dest('dest'));
});

gulp.task('moveJS', function () {
	return gulp
		.src('src/js/*.js')
		.pipe(uglify())
		.pipe(concat('main.js'))
		.pipe(gulp.dest('dest'));
});

gulp.task('clean', function () {
	return del('dest');
});

gulp.task(
	'build',
	gulp.series('clean', gulp.parallel(['moveHTML', 'moveCSS', 'moveJS']))
);
