const gulp = require('gulp');
const concat = require('gulp-concat');
const del = require('del');
const htmlmin = require('gulp-htmlmin');
const autoprefixer = require('gulp-autoprefixer');

gulp.task('copyHTML', function () {
    return gulp
        .src('src/*.html')
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest('public'));
});

gulp.task('copyCSS', function () {
    return gulp
        .src('src/css/**/*.css')
        .pipe(
            autoprefixer({
                cascade: false
            })
        )
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('public'));
});

gulp.task('clean', function () {
    return del('public');
});

gulp.task('build', gulp.series('clean', gulp.parallel(['copyHTML', 'copyCSS'])));

gulp.task('watch', function () {
    gulp.watch('src/**/*', gulp.series('build'));
});
