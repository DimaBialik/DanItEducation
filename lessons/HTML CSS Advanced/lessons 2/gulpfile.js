const gulp = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const rename = require('gulp-rename');

gulp.task('buildStyles', function () {
    return gulp
        .src('./scss/index.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename('style.css'))
        .pipe(gulp.dest('./'));
});

function watchStyles() {
    return gulp.watch('./scss/*.scss', gulp.series('buildStyles'));
}
gulp.task('run', watchStyles);
