'use strict';
// const arr = [1, 'value 2', 43, 4, 5];

// const obj = {
//     info: {
//         name: 'Uasya'
//     },
//     age: 18,
//     count: 10
// };

// console.log(...arr);

// const obj2 = { ...obj, a: 3, c: 4 };

// console.log(obj2);

// function fn(props) {
//     console.log(props);
// }
// fn([1, 3, 'text']);

// function fn({ a, c }) {
//     console.log(a, c);
// }
// fn({ a: 1, c: 4 });

// const data = { a: 1, c: 4 };
// const { a, c, b = 0 } = data;
// console.log(a, c, b);

// const { a: firstProp, b: thirdProp = 0 } = data;
// console.log(firstProp, thirdProp);
// const arr = [undefined, 'value 2', 43, 4, 5];
// const [first = 1, , third, ...rest] = arr;
// console.log(first, third, rest);

/// ---------task 1 ----------------

// class Patient {
//     constructor([height, weight]) {
//         this.height = height / 100;
//         this.weight = weight;
//     }
//     getPatientStatus() {
//         const index = Math.round(this.weight / this.height ** 2);
//         let degree = '';

//         if (index <= 15) {
//             degree = 'анорексия';
//         } else if (index >= 16 && index <= 25) {
//             degree = 'норма';
//         } else if (index >= 26 && index <= 30) {
//             degree = 'лишний вес;';
//         } else if (index >= 31 && index <= 35) {
//             degree = 'I степень';
//         } else if (index >= 36 && index <= 40) {
//             degree = 'II степень';
//         } else if (index >= 41) {
//             degree = 'III степень';
//         }
//         return [index, degree];
//     }
// }
// const patient = new Patient([178, 76]);
// patient.getPatientStatus();
// console.log(patient.getPatientStatus());

/// ---------task 2 ----------------
// class User {
//     constructor(firstName, lastName, ...strings) {
//         this.firstName = firstName;
//         this.lastName = lastName;
//         strings.forEach(elem => {
//             // this[elem.split(':')[0]] = elem.split(':')[1].trim();
//             const [key, value] = elem.split(':');
//             this[key] = value.trim();
//         });
//     }
// }
// const user = new User('Золар', 'Аперкаль', 'status: глава Юного клана Мескии', 'wife: Иврейн');
// console.log(user);

/// ---------task 3 ----------------

// class Warrior {
//     constructor({ name, status, weapon }) {
//         this.name = name;
//         this.weapon = weapon;
//         this.status = status;
//     }
// }
// const warrior = new Warrior({
//     name: 'Uasya',
//     lastName: 'Pupkin',
//     weapon: 'Lopata',
//     status: 'free',
//     vid: 'value12'
// });
// console.log(warrior);

/// ---------task 4 ----------------

const resume = {
    name: 'Илья',
    lastName: 'Куликов',
    age: 29,
    city: 'Киев',
    skills: [
        { name: 'Vanilla JS', practice: 5 },
        { name: 'ES6', practice: 3 },
        { name: 'React + Redux', practice: 1 },
        { name: 'HTML4', practice: 6 },
        { name: 'CSS2', practice: 6 }
    ]
};

const vacancy = {
    company: 'SoftServe',
    location: 'Киев',
    skills: [
        { name: 'Vanilla JS', experience: 3 },
        { name: 'ES6', experience: 2 },
        { name: 'React +Redux', experience: 2 },
        { name: 'HTML4', experience: 2 },
        { name: 'CSS2', experience: 2 },
        { name: 'HTML5', experience: 2 },
        { name: 'CSS3', experience: 2 },
        { name: 'AJAX', experience: 2 },
        { name: 'Webpack', experience: 2 }
    ]
};
class Percentage {
    constructor(resume, vacancy) {
        this.resume = resume;
        this.vacancy = vacancy;
    }
    getPercentage() {
        const { skills: skillResume } = this.resume;
        const { skills: skillVacancy } = this.vacancy;
        console.log(skillResume, skillVacancy);
        let counter = 0;
        // skillVacancy.forEach(({ name: nameVacancy, experience }) => {
        //     skillResume.forEach(({ name: nameResume, practice }) => {
        //         if (nameVacancy === nameResume && experience <= practice) {
        //             counter++;
        //         }
        //     });
        // });
        skillVacancy.forEach(({ name: nameVacancy, experience }) => {
            if (
                skillResume.find(
                    ({ name: nameResume, practice }) =>
                        nameVacancy === nameResume && experience <= practice
                )
            ) {
                counter++;
            }
        });

        return (counter * 100) / skillVacancy.length;
    }
}
const percentage = new Percentage(resume, vacancy);
console.log(percentage.getPercentage());
