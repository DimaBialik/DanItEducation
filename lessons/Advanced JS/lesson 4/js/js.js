// try{}....catch(){}-------------

// try {
//     function fn(b) {
//         return b.remove();
//     }
//     console.log(fn('r'));
//     console.log('here');
// } catch (e) {
//     // console.dir(e);

//     console.dir(e.message);
// }
// console.log('what?!');

// function fn(b) {
//     try {
//         return b.remove();
//     } catch (e) {
//         // console.dir(e);
//         throw new Error(e.message);
//         throw { name: 'Error' };
//     } finally {
//         console.log('www');
//     }
// }
// try {
//     console.log(fn('r'));
// } catch (e) {
//     console.log(e);
// }

// console.log(fn('r'));
// console.log('here');

// console.log('what?!');

//const root = document.getElementById('root') || document.createElement('div');

// {
//     let a = 2; // -блочная область видeмости
// }
// console.log(a);

// IIFE
// let a = 5;
// (function (c) {
//     let a = 43;
//     console.log(c);
//     console.log(a);
// })(a);

//-------------- task 1 -------------------

class ChangeTextSize {
    constructor(sizeText) {
        this.sizeText = sizeText;
        this.sizeBefore = parseFloat(this.sizeText.style.fontSize);
    }

    increase(size) {
        this.sizeBefore += size;
        this.sizeText.style.fontSize = this.sizeBefore + 'px';
    }
    decrease(size) {
        this.sizeBefore -= size;
        this.sizeText.style.fontSize = this.sizeBefore + 'px';
    }
}
const text = document.querySelector('#text');
const increaseBtn = document.querySelector('#increase-text');
const decreaseBtn = document.querySelector('#decrease-text');
const changeTextSize = new ChangeTextSize(text, 12);

increaseBtn.addEventListener('click', () => {
    changeTextSize.increase(2);
});

decreaseBtn.addEventListener('click', () => {
    changeTextSize.decrease(3);
});

//-------------- task 2 -------------------

class Product {
    constructor(name, fullName, article, price) {
        this.name = name;
        this.fullName = fullName;
        this.article = article;
        this.price = price;
        Object.defineProperties(this, 'price', {
            get: function () {
                return this.price;
            },
            set: function () { }
        });
    }
    // set price(price) {
    //     return price.checkPrice();
    // }
    // checkPrice() {
    //     if (Number.isSafeInteger(price)) {
    //         return price;
    //     }
    // }
}
class createProduct {
    constructor(name, fullName, article, price) {
        this.name = name;
        this.fullName = fullName;
        this.article = article;

        this.price = price;
        Object.defineProperty(this, 'price', {
            get: function () {
                return price;
            },
            set: function (newPrice) {
                if (Number.isInteger(newPrice) && newPrice > 0) {
                    price = newPrice;
                }
            },
            enumerable: true,
            configurable: true
        });
    }
}
