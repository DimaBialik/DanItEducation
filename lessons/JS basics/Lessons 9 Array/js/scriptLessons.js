const arr = [1,23,4,6,8,2];
// arr.length ---> длина масива 
// #1


const arr1 = ['jazz','Blues'];
arr1.push('Rock-n-Roll');
const middleElementIndex = Math.floor(arr1.length  / 2);
arr1.splice(middleElementIndex,1, 'Classics');
console.log(arr1[0]);
arr1.unshift('Rap', 'Reggae');
console.log(arr1);


 //  forEach
 console.log('--------------->> forEach');
 arr.forEach((e,i) => { 
    if (e%2 ===0){
        console.log(e);
    }
 });

// map 
console.log('---------------> map');
const newArrFromArr = arr.map((e,i) => { 
    if (i !==2){ 
        return e;
    }
    // ....
    return e+2
 });
 console.log(newArrFromArr);

 // #2 
 console.log('---------------> task 2');
 const array = [1,2,3,4,5,6,7,8,9,10];

 array.forEach((e) => { 
if (e %3===0){ 
    console.log(e);
}
 })

 const newArray = array.map((e,i)=> { 
     return { index: i,
     number : e};
 })
 console.log(newArray);

// filter 
console.log('---------------> filter');

const filterArray = array.filter((e,i) => { 
    if (e %3===0){ 
       return e;
    }
    
})
console.log(filterArray);
// #3
console.log('---------------> task 3');

const array1 = [];
for (let i =0; i<=100;i++){ 
    array1.push(i);
}
console.log(array1)
const filterArray1 = array1.filter((e) => { 
    if (e>50 &&  e<70 ){ 
      return e ;
    } 
})
console.log(filterArray1)




// #4
console.log('---------------> task 4');
// Описание задачи: Напишите функцию, которая очищает массив от нежелательных значений,
// таких как false, undefined, пустые строки, ноль, null.

// const data = [0, 1, false, 2, undefined, '', 3, null];
// console.log(compact(data)) // [1, 2, 3]
const data = [0, 1, false, 2, undefined, '', 3, null, 'asdada'];

const compact = data.filter(e=> e);
console.log(compact);


// #5
console.log('-----> task 5');

// Есть массив брендов автомобилей ["bMw", "Audi", "teSLa", "toYOTA"].
// Вам нужно получить новый массив, объектов типа
// {
//     type: 'car'
//     brand: ${элемент массива}
// }
// Для всех машин добавить свойство isElectric, для теслы оно будет true, для остальных false;
// Для того чтоб проверять что машина электрическая создать отдельную функцию что будет хранить
// в себе массив брендов электрических машин и будет проверять входящий элемент - на то,
// если он в массиве.

const  chechIsElectric = ( brand) => { 
    const isElectric = [ 'tesLa'];               // include 
   
   return  isElectric.map((e) => e.toLowerCase()).includes(brand);
}

const carArray = ["bMw", "Audi", "teSLa", "toYOTA"];
 const newCarArra = carArray.map((e,i) => { 
     return {
            type: 'car',
           brand: e,
           isElectric: chechIsElectric(e),
        }
 });
 console.log(newCarArra);




 // split 
console.log('---------------> split');

const str = 'hello, my name is, and , sadas'
console.log(str.split(','));

// функция палиндром
// anina -> split()
// [a n i n a] -> reverse()
// [a n i n a] -> join()
// anina

// #6 
console.log('---------------> task 6');

// Описание задачи: Напишите функцию, которая сравнивает два массива и возвращает true,
// если они идентичны.
// Ожидаемый результат: ([1, 2, 3], [1, 2, 3]) => true

// const arr1 = [1, 2, 3, 4];
// const arr2 = [1, 2, 3, 4];
// const arr3 = [1, 2, 3, 5];
// const arr4 = [1, 2, 3, 4, 5];
// console.log(isEqual(arr1, arr2)); // true
// console.log(isEqual(arr1, arr3)); // false
// console.log(isEqual(arr1, arr4)); // false


 //rest 
 console.log('---------------> split');

 // filter find isArray join  map push  reduce some every slice splice 