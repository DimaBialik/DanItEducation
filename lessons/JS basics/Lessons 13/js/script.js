// document.addEventListener('DOMContentLoaded', () => {
//     console.log('DOMContentLoaded');
// });
// window.addEventListener('load', () => {
//     console.log('window loaded');
// });

// // window.onbeforeunload = () => {
// //     console.log('window onbeforeunload');
// //     return 'sadada';
// // };
// let h1 = document.querySelector('h1');
// h1.addEventListener('onclick', e => {
//     e.target.style.colore = 'red';
// });

// document.addEventListener('keydown', e => {
//     console.log('keydown ', e.key, e.code);
// });
// document.addEventListener('keyup', e => {
//     console.log('keyup ', e.key, e.code);
// });
// window.addEventListener('scroll', e => {
//     e.preventDefault();
//     console.log('scroll', e);
// });

// let myInput = document.querySelector('#myInput');
// myInput.addEventListener('keydown', e => {
//     if (e.code === 'Enter') {
//         console.log(myInput.value);
//     }
//     if ((e.metaKey || e.ctrlKey) && e.code === 'KeyZ') {
//         console.log('ctrl+Z pressed');
//     }
// });

/**
 * ------------------------Задание 1.
 *
 * Создать элемент h1 с текстом «Нажмите любую клавишу.».
 *
 * При нажатии любой клавиши клавиатуры менять текст элемента h1 на:
 * «Нажатая клавиша: ИМЯ_КЛАВИШИ».
 */

let myH1 = document.createElement('h1');
myH1.innerText = 'Нажмите любую клавишу';
document.body.append(myH1);
document.addEventListener('keydown', e => {
    // console.log(e);
    myH1.innerText = `Нажатая клавиша:  ${e.key}`;
    console.log(e.key);
});

/**
 *  * -------------------------Задание 2
 * Рахувати кількість натискань на пробіл, ентер,
 * та Backspace клавіші
 * Відображати результат на сторінці
 *
 */

// let enterNum = document.querySelector('#enter-counter');
// let spaceNum = document.querySelector('#space-counter');
// let backspaceNum = document.querySelector('#backspace-counter');
// let counter1 = 0;
// let counter2 = 0;
// let counter3 = 0;
// // enterNum.innerText = Number(0);
// document.addEventListener('keydown', e => {
//     if (e.code === 'Enter') {
//         counter1++;
//         enterNum.innerText = counter1;
//     } else if (e.code === 'Space') {
//         counter2++;
//         spaceNum.innerText = counter2;
//     } else if (e.code === 'Backspace') {
//         counter3++;
//         backspaceNum.innerText = counter3;
//     }
// });
/**
 * * -------------------------Задание 3
 * При натисканні shift та "+"" одночасно збільшувати
 * шрифт сторінки на 1px
 * а при shift та "-" - зменшувати на 1px
 *
 * Максимальний розмір шрифту - 30px, мінімальний - 10px
 *
 */

// let fontSize = 16;
// window.addEventListener('keyup', e => {
//     if (e.shiftKey && e.key === '+') {
//         fontSize++;
//         document.body.style.fontSize = fontSize + 'px';
//     }
//     if (e.shiftKey && e.key === '-') {
//         fontSize--;
//         document.body.style.fontSize = fontSize + 'px';
//     }
// });

/**
 * * -------------------------Задание 4
 * При натисканні на enter в полі вводу
 * додавати його значення, якщо воно не пусте,
 * до списку задач та очищувати поле вводу
 *
 * При натисканні Ctrl + D на сторінці видаляти
 * останню додану задачу
 *
 * Додати можливість очищувати весь список
 * Запустити очищення можна двома способами:
 * - при кліці на кнопку Clear all
 * - при натисканні на Alt + Shift + Backspace
 *
 * При очищенні необхідно запитувати у користувача підтвердження
 * (показувати модальне вікно з вибором Ok / Cancel)
 * Якщо користувач підвердить видалення, то очищувати список,
 * інакше нічого не робити зі списком
 *
 */

const input = document.querySelector('#new-task');
const list = document.querySelector('#task');
const btnClear = document.querySelector('#clear');

input.addEventListener('keydown', e => {
    if (e.key === 'Enter' && input.value !== '') {
        list.insertAdjacentHTML('beforeend', `<li>${input.value}</li>`);
        input.value = '';
    }
});
window.addEventListener('keydown', e => {
    if ((e.ctrlKey || e.metaKey) && e.code === 'KeyM') {
        if (list.lastElementChild) {
            list.lastElementChild.remove();
        }
    }
});
window.addEventListener('keydown', e => {
    if (e.code === 'Backspace' && e.shiftKey && e.altKey) {
        deleteAllConfirm();
    }
});

function deleteAllConfirm() {
    let result = confirm('are u sure?');
    if (result) {
        list.innerHTML = '';
    }
}
btnClear.onclick = function () {
    list.innerHTML = '';
};
