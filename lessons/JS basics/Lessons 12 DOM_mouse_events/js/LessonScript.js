// ---------task 1
// const btn = document.querySelector('#hider');
// btn.onclick = function () {
//
//     const textToHideDisplay = window.getComputedStyle(textToHide).display;
//     //     display = 'none' ? visible : none;
//     textToHide.style.display = textToHideDisplay === 'none' ? 'block' : 'none';

// ---------task 2
// const btn = document.querySelector('#hider');
// btn.onclick = function () {
//     this.remove();
//     // this.getElementsByClassName.display = 'none';  --не работает
// };

// ---------task 3

const incrementBtn = document.querySelector('#increment');
const counter = document.querySelector('#counter');
const resetBtn = document.querySelector('#reset');

function increment() {
    if (counter.innerText < 10) {
        counter.innerText = Number(counter.innerText) + 1;
    } else {
        counter.innerText = Number(counter.innerText) * 10;
    }
}
function reset(e) {
    console.log(e);
    counter.innerText = 1;
}

incrementBtn.addEventListener('click', increment);
resetBtn.addEventListener('click', reset);

selfCounter.addEventListener('click', e => {
    e.target.innerText = +e.target.innerText + 1;
});

// myInput.addEventListener('onchange', e => {
//     console.log(e);
// });

selfCounter.addEventListener('mouseover', e => {
    e.target.style.backgroundColor = 'green';
});
selfCounter.addEventListener('mouseleave', e => {
    e.target.style.backgroundColor = 'red';
});
const cordinates = document.querySelector('#cordinates');

window.addEventListener('mousemove', e => {
    cordinates.innerText = `X:${e.clientX} Y:${e.clientY}`;
});

// ---------task 4

/**
 * Задание 3.
 *
 * Создать элемент h1 с текстом «Добро пожаловать!».
 *
 * Под элементом h1 добавить элемент button c текстом «Раскрасить».
 *
 * При клике по кнопке менять цвет каждой буквы элемента h1 на случайный.
 */

/* Дано */
const PHRASE = 'Добро пожаловать!';

function getRandomColor() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);

    return `rgb(${r}, ${g}, ${b})`;
}

let welcomText = document.createElement('h1');
const colorBtn = document.createElement('button');
colorBtn.innerText = 'Раскрасить';

for (let i of PHRASE) {
    let span = document.createElement('span');

    span.innerText = i;
    welcomText.append(span);
}
document.body.prepend(welcomText);

welcomText.after(colorBtn);

const spanAll = document.querySelectorAll('span');

colorBtn.onclick = function () {
    spanAll.forEach(el => {
        el.style.color = getRandomColor();
    });
};

// ---------task 5
/**
 * При наведенні на блок 1 робити
 * блок 2 зеленим кольором
 * А при наведенні на блок 2 робити
 * блок 1 червоним кольором
 *
 */

let block1 = document.querySelector('.block1');
let block2 = document.querySelector('.block2');

block1.addEventListener('mouseover', () => {
    block2.style.backgroundColor = 'green';
});
block2.addEventListener('mouseover', () => {
    block1.style.backgroundColor = 'red';
});
block1.addEventListener('mouseleave', () => {
    block2.style.backgroundColor = 'white';
});
block2.addEventListener('mouseleave', () => {
    block1.style.backgroundColor = 'white';
});
