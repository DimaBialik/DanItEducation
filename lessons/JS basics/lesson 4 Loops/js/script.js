/*
// вывести числа кратны 2 и 10 в промежутке от 0 до 100

for (let i = 0; i <= 100; i++) {
	if (i % 2 === 0) {
		console.log(i);
	}
}

// even numbers 0 -> 10

let i = 20;

while (i <= 10) {
	if (i % 2 === 0) {
		console.log(i);
	}
	i++;
}

do {
	if (i % 2 === 0) {
		console.log(i);
	}
	i++;
} while (i <= 10);

// просить юзера ввести число пока не введёт 10
let number = null;
do {
	number = +prompt('Enter 10');
} while (number !== 10);

// continue, break

for (let i = 0; i <= 10; i++) {
	if (i < 5) {
		continue;
	}
	console.log(i);
}

for (let i = 0; i <= 10; i++) {
	if (i < 5) {
		continue;
	}
	console.log(i);
}

// loop 0 -> 9, 0123456789, записывать пока длина строки <=5

let myStr = ''; //myStr.length === 5
// 's' + 's' -> 'ss'

for (let i = 0; i < 10; i++) {
	myStr += String(i);
	if (myStr.length > 5) {
		break;
	}
}

// 100 90 80 70 60 50 40 30 20 10 0

for (let i = 100; i >= 0; i -= 10) {
	console.log(i);
}

// Task 1
// вывести числа (от5 до 100) кот дел одноврем на 2 или на 3

for (let i = 5; i <= 100; i++) {
	if (i % 2 === 0 || i % 3 === 0) {
		console.log(i);
	}
}

// Task 2
// пользователь вводит 2 числа
let num1 = null;
let num2 = null;
do {
	num1 = prompt('Enter num 1');
} while (num1 === '' || Number.isNaN(+num1));
do {
	num2 = prompt('Enter num 2');
} while (num2 === '' || Number.isNaN(+num2));
console.log(num1);
console.log(num2);

// Task 3
// пользователь вводит И Ф( не пустая строка проверка) и ДР (проверка >1950)

let name3;
let surName;
let age;
do {
	name3 = prompt('Enter name');
} while (name3 === '');
do {
	surName = prompt('Enter surname');
} while (surName === '');
do {
	age = +prompt('Enter age');
} while (Number.isNaN(age) || age < 1950);

*/

// Task 4.

let userName = null;
let grade = null;
let gradeLetter = null;

do {
	userName = prompt('input name');
} while (!userName || !userName.trim());

do {
	grade = +prompt('input grade');
} while (!grade || Number.isNaN(grade) || grade < 0 || grade > 100);

if (grade >= 95) {
	gradeLetter = 'A';
} else if (grade >= 90) {
	gradeLetter = 'A-';
} else if (grade >= 85) {
	gradeLetter = 'B+';
} else if (grade >= 80) {
	gradeLetter = 'B';
} else if (grade >= 75) {
	gradeLetter = 'B-';
} else if (grade >= 70) {
	gradeLetter = 'C+';
} else if (grade >= 65) {
	gradeLetter = 'C';
} else {
	gradeLetter = 'F';
}
alert(`to student ${userName} grade is ${gradeLetter}`);
