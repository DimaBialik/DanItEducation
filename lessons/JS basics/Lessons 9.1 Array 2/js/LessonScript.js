// ------->> Rest spread
function rand(...arg){ 
    //....
};

const arr23=[1,2];
const copyOfArr23 = [...arr23];
// console.log(...arr23, ...arr23);


// ------->> task 1 

// const arr = [1,2,3,4,5];

const cloneArray = (arr) => {
// -----------------    1 spread

//  return  [...arr];

// -----------------    2 loop

// const cloneArr =[];

//      for(let i =0;i< arr.length;i++ ) { 
//         cloneArr.push(arr[i]);
//      }
//        return cloneArr;
    
// -----------------    3 map 

// return arr.map((e) =>e); // -->  const cloneArr1 = arr.map(e => e);
   
// -----------------    4  slice 

// return arr.slice(0); //-->  return arr.slice();
}

// ------->> task 2 

/*Task 1.
 
  Написать функцию-помощник кладовщика replaceItems.
 
  Функция должна заменять указанный товар новыми товарами.
 
  Функция обладает двумя параметрами:
  - Имя товара, который необходимо удалить;
 *- Список товаров, которыми необходимо заменить удалённый товар.
 
  Функция не обладает возвращаемым значением.
 
  Условия:
  - Генерировать ошибку, если имя товара для удаления не присутствует в массиве;
  - Генерировать ошибку, список товаров для замены удалённого товара не является массивом.
 
  Заметки:
 *- Дан «склад товаров» в виде массива с товарами через.
 /

/* Дано */
    let storage = [
    'apple',
    'water',
    'banana',
    'pineapple',
    'tea',
    'cheese',
    'coffee',
    ];
// splice
    
    const replaceItems = (itemRemove,  newItem) => { 
        
        const checkProduct = storage.indexOf(itemRemove);
        if (checkProduct ===-1 ){ 
            alert('error');
            return;
        }

        storage.splice(checkProduct, 1, ...newItem );
        
        return storage;

        // return storage.splice(storage.indexOf(itemRemove), 1, newItem);

    };
    // let itemRemove = prompt( 'enter product to remove');
    // if ( storage.indexOf(itemRemove)) === -1) { 

    // }
    console.log( replaceItems('water', ['rangs', 'smth']));



    // ------->> task 3 
    /* TASK - 2
* Create function getItemList(), that has to receive string from the user with the items names, separated by the ', '.
* Items in the users string can be repeated.
* User string needs to be transformed into an array with unique values.
*/

// const arrSet= ['apple', 'orange', 'orange','banana', 'pineapple' ];
const getItemList = (str) => { 
const uniqueSetObj =  new Set(str.split(', '))
console.log([...uniqueSetObj]); 
console.log(Array.from(uniqueSetObj));
}
getItemList('apple, orange, orange, banana, pineapple');

// console.log(new Set(['apple', 'orange', 'orange','banana']));


const arrNew = [1,2,3,21,56];
console.log([4,5,...arrNew]);
console.log(...arrNew);

// --------> Reduce

const sumArrNew = arrNew.reduce((acc, el, index, arr)=> { 
console.log('-->',acc, el);
return el+acc
}, 0)
console.log(sumArrNew);



 // ------->> task 4
const midle = arrNew.reduce((acc,el,index,arr) => { 
    acc+=el;
    if (index === arr.length-1){ 
        return acc/arr.length

    }
   return acc
},0)
console.log(midle);



 // ------->> task 5

 
 const sumArray = arrNew.reduce((acc,el) => { 
    
    if (el > 5){ 
        return acc +el;
    } 
        return acc
    
   
},0)
console.log(sumArray);

// ------->> task 6 
// 
// Вернуть объект где ключ это название продукта, а значение это его кол-во

const fruitBasket = ['banana', 'cherry', 'orange', 'apple', 'cherry', 'orange', 'apple', 'banana', 'cherry', 'orange', 'fig' ];


const findOfObj = fruitBasket.reduce((acc,el,index,arr) => { 
 if (el in acc){ 
     acc[el] +=1;
 } else { 
     acc[el]=1
 }
  return acc

},{})
console.log(findOfObj);