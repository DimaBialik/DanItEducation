const a = '14';
const b = 5;
const c = 5;

console.log(Number(a) + b); // 145 || 19
console.log(+a + b); // 145 || 19
console.log(parseInt(a) + b); // 145 || 19

console.log(+a * +b);

console.log(String(b) + String(c));
console.log(`${b}${c}`);

let myNumber = 14;
const myString = 'asd';
const myBool = true;
const myNl = null;
const myUndef = undefined;
const myArray = [1, 3, 2];
const myObject = {
	name: 'Oleksandr',
	age: 22,
	work: 'Kyivstar',
};
const myNaN = 2 / 'au';

console.log('------------ To Number');

console.log(Number(myNumber)); // number
console.log(Number(myString)); // 'saf' -> NaN, '' -> 0
console.log(Number(myBool)); // true = 1, false = 0
console.log(Number(myNl)); // 0
console.log(Number(myUndef)); // NaN
console.log(Number(myArray)); // NaN
console.log(Number(myObject)); // NaN
console.log(Number(myNaN)); // NaN

console.log('------------ To String');

console.log(String(myNumber)); // string
console.log(String(myString)); // string
console.log(String(myBool)); // string
console.log(String(myNl)); // string
console.log(String(myUndef)); // string
console.log(String(myArray)); // string
console.log(String(myObject)); // [object Object]
console.log(String(myNaN)); // string

console.log('------------ To Boolean');

console.log(Boolean(myNumber)); // >0 true, =0 false
console.log(Boolean(myString)); // 'wqe' true, '' false
console.log(Boolean(myBool)); // bool
console.log(Boolean(myNl)); // false
console.log(Boolean(myUndef)); // false
console.log(Boolean(myArray)); // true
console.log(Boolean(myObject)); // true
console.log(Boolean(myNaN)); // false

myNumber++; // myNumber = myNumber + 1; // myNumber += 1;
myNumber = 3;
console.log(Math.pow(myNumber, 3));
console.log(myNumber);
const negNumb = -3;
const posNumber = Math.abs(negNumb); // 3

const age = 4.0;
console.log(Math.round(age));
console.log(Math.floor(age));
console.log(Math.ceil(age));

const INITIAL_NUMBER = 'qwe';

console.log(!INITIAL_NUMBER);
console.log('-----------------------------------------');
console.log('5' == 5); // true
console.log(5 === '5'); // false
console.log('' == 0); // true
console.log('' == []); // true
console.log({} == []); // false

console.log(21000000000);
console.log(21e9);
const num = 324.32432523;
console.log(+num.toFixed(3));
