function pow(x, n, callback) {
	let res = 1;
	for (let i = 0; i < n; i++) {
		res *= x;
	}
	callback(res);
}

function customAlert(str) {
	alert(`Your result is ${str}`);
}

function customSum(a, b) {
	const sum = a + b;
	return sum;
}

const func1 = function customFuncName1(a, b) {
	//...
};

const func2 = function () {
	//...
};

function multiply(a, b) {
	return a * b;
}
multiply(3, 4);

pow(3, 4, customAlert);
const sum1 = customSum(3, 4);

// 1
// min(a,b) -> return min value between a and b.

function getMin(a, b) {
	if (a < b) {
		return a;
	} else if (b < a) {
		return b;
	} else {
		return 'a =b';
	}
}

// getMin(5, 7);
console.log(getMin(3, 3));

// 2
// Функция count принимает 2 числа
// первое число - откуда нужно начать счёт
// второе число - где остановить счёт
// первое число не может быть больше либо равно второму
// выводить числа в консоль начиная от первого до второго
// когда счёт закончен вывести сообщение "Отсчёт окончен"

// function count(a, b) {
// 	if (a > b) {
// let c = a;
// a = b;
// b = c;
// 	}

// 	for (i = a; i <= b; i++) {
// 		console.log(i);
// 	}
// 	console.log('Отсчет окончен');
// }

// count(13, 6);
// count(6, 13);

// 3
// Юзер вводит 2 числа, проверить оба числа на валидность (оформить через функцию)
// если юзер ввёл два числа в результате вывести их сумму

// let numOne;
// let numTwo;
// do {
// 	numOne = prompt('Enter numOne');
// } while (check(numOne));

// do {
// 	numTwo = prompt('Enter numTwo');
// } while (check(numTwo));

// console.log(+numOne + +numTwo);

// function check(number) {
// 	return number === '' || Number.isNaN(+number);
// }

// 4
// написать функцию сумматор, которая возвращает
// сумму всех чисел которые были переданы в функцию
// advancedSum(4, 5, 'asf', null, {}, [1,2], 56) -> 65

// function advancedSum() {
// 	let sum = 0;
// 	console.log(arguments);
// 	for (let arrayElem of arguments) {
// 		if (typeof arrayElem === 'number') {
// 			sum += arrayElem;
// 		}
// 	}
// 	return sum;
// }

// advancedSum(4, 5, 'asf', null, {}, [1, 2], 56);

// console.log('Simple log');
// console.warn('Warn log');
// console.error('Error log');
