// const el = document.querySelector('h1');
// console.log(el.getAttribute('data-wiget-name'));
// console.log(el.dataset.wigetName);
// el.removeAttribute('data-wiget-name');

// const squeare = document.querySelector('.squeare');
// console.log(window.getComputedStyle(squeare).getPropertyValue('width'));

// const div = document.createElement('div');
// div.innerText = 'my Div';
// el.append(div);

// el.after(div);
// el.insertAdjacentHTML;

//-------------> task 1
/**
 * Задание
 *
 * Написать скрипт, который создаст квадрат произвольного размера.
 *
 * Размер квадрата в пикселях получить интерактивно посредством диалогового окна prompt.
 *
 * Если пользователь ввёл размер квадрата в некорректном формате —
 * запрашивать данные повторно до тех пор, пока данные не будут введены корректно.
 *

 * Тип элемента, описывающего квадрат — div.
 * Задать ново-созданному элементу CSS-класс .square.
 *
 * Квадрат в виде стилизированного элемента div необходимо
 * сделать первым и единственным потомком body документа.
 */

// let side = prompt('enter side of square');

// while (!side || Number.isNaN(+side)) {
//     side = prompt('enter side of square');
// }
// let square = document.createElement('div');

// square.style.cssText = `
// width = ${side}px;
// height = ${side}px;
// background-color = red`;

// document.body.append(square);

//-------------> task 2
// const list = document.querySelector('#list');
// let product = prompt('enter some product');

// while (product !== null && product.trim()) {
//     let li = document.createElement('li');
//     li.innerText = product;
//     list.append(li);
//     product = prompt('enter some product');
//     li.onclick = function () {
//         this.remove();
//     };
// }

//-------------> task 3

// Задание

// Программа будет выводить то или иное сообщение на страницу в зависимости от возраста пользователя.
// Сообщение представляет из себя html-элемент div. В нем будет сообщение (далее alert-message) и тип
// (тип алерта - это тот или иной класс для div).

// Логика:
// Спросить у пользователя его имя и возраст.
// Если число (также проверить, что там число) меньше 18, вывести на страницу
// сообщение, где alert-message - Sorry, you are not be able to visit us. Тип - 'alert-danger'
// Если больше 18 - alert-message 'Hi, ${name}.', тип 'alert-success';

// let name = prompt('enter u name');
// let age = +prompt('enter u age');
// let massage;
// let type;
// if (age >= 18) {
//     massage = `hi ${name}`;
//     type = 'alert-success';
// } else {
//     massage = 'Sorry, you are not be able to visit us';
//     type = 'alert-danger';
// }
// const elem = document.querySelector('div');
// elem.innerText = massage;
// elem.classList.add(type);
// document.body.append(elem);

//-------------> task 4

// По клику на кнопку считывать значение атрибута value (введеный текст).
// введеный текст вставлять после кнопки.

function addTextOnPage() {
    const input = document.querySelector('input');
    const el = document.querySelector('#newItemsContainer');
    el.innerText = input.value;
}
