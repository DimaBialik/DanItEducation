// const heading = document.body.lastElementChild;
// console.log(heading);
// // for (let elem of heading){ 
// //     console.log(elem);
// // }
// //  firstElementChild lastElementChild nextSibling 
// const bodyFirstChild = document.body.firstElementChild;
// const bodyLastChild = document.body.lastElementChild;

// console.log(bodyFirstChild.nextElementSibling);
// console.log(bodyLastChild.previousElementSibling);


// console.log(document.querySelector('h2')); // console.log(document.querySelector('h2+h3>h4'));
// console.log(document.querySelector('#heading2'));
// console.log(document.querySelector('.div'));
// console.log(document.getElementsByClassName('div')[0]);
// console.log(document.getElementById('heading2'));
// console.log(document.getElementsByTagName('h2')[0]);
// // console.log(document.getElementsByName('h2')[0]);

// document.querySelector('h2');
// document.querySelectorAll('h2');



// const myHeading2 = document.querySelector('h2');
// myHeading2.style.color = 'red';

// const myHeading2Copy = myHeading2.cloneNode(true); // копирует елемент
// document.body.prepend(myHeading2Copy)    // переносит в начало html


// task 1

// console.log(document.querySelector('div')); //1

// console.log(document.querySelector('ul')); //2

// console.log(document.getElementsByTagName('li')[1]); //3


// task 2 

// const table = document.querySelector('table');
// console.log(table.rows);

// for (let i = 0; i < table.rows.length; i++) {
//     console.log(table.rows[i])
//     table.rows[i].cells[i].style.background = 'red'
// };

// task 3

/**
 * Задание.
 *
 * Получить и вывести в консоль следующие элементы страницы:
 * - По идентификатору (id): элемент с идентификатором list;
 * - По классу — элементы с классом list-item;
 * - По тэгу — элементы с тэгом li;
 * - По CSS селектору (один элемент) — третий li из всего списка;
 * - По CSS селектору (много элементов) — все доступные элементы li.
 *
 * Вывести в консоль и объяснить свойства элемента:
 * - innerText;
 * - innerHTML;
 * - outerHTML.
 */


// const listIdElement = document.getElementById('list');
// console.log(listIdElement);

// const listItemClass = document.getElementsByClassName('list=item');
// console.log(listItemClass);

// const liTagElements =document.getElementsByTagName('li');
// console.log(liTagElements);

// const liThird = document.querySelectorAll('li')[2]; // querySelector('li: nth-child(2')
// console.log(liThird);

// const liAll =document.querySelectorAll('li');
// console.log(liAll);

// console.log(liThird.innerText);
// console.log(liThird.innerHTML);



// task 4
/**
 * Задание.
 
 * Получить элемент с классом .bigger.
 * Заменить ему CSS-класс .bigger на CSS-класс .active.
 *
 * Условия:
 * - Вторую часть задания решить в двух вариантах: в одну строку и в две строки.
 */

// const biggerItem = document.querySelector('.bigger');
// console.log(biggerItem);

// biggerItem.classList.replace('bigger', 'active'); // replace --> заменил 1 на 2 
// // biggerItem.classList.remove('bigger')-->удалил  biggerItem.classList.add('active')--Юдобавил

 




// task 5 

/**
 * Задание.
 *
 * На экране указан список товаров с указанием названия и количества на складе.
 *
 * Найти товары, которые закончились и:
 * - Изменить 0 на «закончился»;
 * - Изменить цвет текста на красный;
 * - Изменить жирность текста на 600.
 *
 * Требования:
 * - Цвет элемента изменить посредством модификации атрибута style.
 */

// const storeProduct = document.querySelectorAll('li');

// storeProduct.forEach((li) =>{ 
// const findElem =li.innerText.includes(': 0');
// if (findElem) { 
//    li.innerText =  li.innerText.replace(0, 'end');
// //    li.style.color = 'red';
// //    li.style.fontWeight = '600';
// li.classList.add('out-of-stock')
// }
// });

// task 6 

/**
 * Задание.
 *
 * Получить элемент с классом .list-item.
 * Отобрать элемент с контентом: «Item 5».
 *
 * Заменить текстовое содержимое этого элемента на ссылку, указанную в секции «дано».
 *
 * Сделать это так, чтобы новый элемент в разметке не был создан.
 *
 * Затем отобрать элемент с контентом: «Item 6».
 * Заменить содержимое этого элемента на такую-же ссылку.
 *
 * Сделать это так, чтобы в разметке был создан новый элемент.
 *
 * Условия:
 * - Обязательно использовать метод для перебора;
 * - Объяснить разницу между типом коллекций: Array и NodeList.
 */

const targetElem = `<a href="https://ww.google.com">Google it</a>`;

 const findItem = document.querySelectorAll('.list-item');

 findItem.forEach((li) =>{ 
if (li.innerText === 'Item 5'){ 
li.innerText =targetElem;
}else if(li.innerText === 'Item 6'){ 
    li.innerHTML=targetElem
}

}
);
