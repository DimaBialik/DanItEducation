// // ! && ||

// // && (last true, first false)

// // 5 && true -> True
// // "" && 1 -> ""
// // false && true || 0 -> False
// // 5 && [] -> []
// // {} && "" && 0 -> ""

// // || (last false, first true)

// // false || 5 -> 5
// // false || 0 -> 0
// // "" || 5 || 6 -> 5

// // ! (to Boolean)

// // !true -> False
// // !5 -> False
// // !"" -> True

// // 5 && 0 || 6 -> 6
// // 5 || 0 && 6 -> 5
// // !5 || 0 && 6 -> 0
// // !5 && !0 && "" -> false
// // (0 || 5) && ("" || 1) -> 1
// // 5 && 0 && (7 || [] || false) -> 0
// // !(5 && 0 && (7 || [] || false)) -> true
// // [] && {} || "" && 5 -> {}
// // 0 || "" && "" && 1 -> ""
// // (5 || true || {} && 0) && (!22 || 22 && 5) -> 5

// const a = +prompt('Give a');
// const b = +prompt('Give b');

// // == ===
// // != !==
// // > < >= <=

// if (a > b) {
// 	alert('A');
// } else if (a < b) {
// 	alert('B');
// } else {
// 	alert('A=B');
// }

// const message2 = a > b ? alert('A') : a < b ? alert('B') : alert('A=B');

// let message = '';
// if (a >= 18) {
// 	message = 'Welcome';
// } else {
// 	message = 'You are not allowed';
// }

// const message = a >= 18 ? 'Welcome' : 'You are not allowed';

// const name = prompt('Enter name');

// // if (name === 'Max') {
// // 	alert('Hi, reader');
// // } else if (name === 'Mike') {
// // 	alert('Hi, editor');
// // } else if (name === 'Add') {
// // 	alert('Hi, God');
// // }

// switch (name) {
// 	case 'Max':
// 		alert('Hi, reader');
// 		break;
// 	case 'Mike':
// 		alert('Hi, editor');
// 		break;
// 	case 'Add':
// 		alert('Hi, God');
// 		break;
// 	default:
// 		alert('Hi, Incognito');
// }

// Task 1
// юзер вводит число: если 0-20 - то текст "Число ${usersNumber} побежд",
// иначе - "Сорри? ты проиграл"

// const usersNumber = +prompt('Введите число');

// if (!Number.isNaN(usersNumber)) {
// 	if (usersNumber >= 0 && usersNumber <= 20) {
// 		alert(`Ваше число ${usersNumber} победило!`);
// 	} else {
// 		alert('Сорри? ты проиграл');
// 	}
// } else {
// 	alert('Вы ввели не число');
// }

// Task 2
//юзер вводит число: четное? нечетное?

// const usersNumber = +prompt('Введите число');

// if (!Number.isNaN(usersNumber)) {
// 	if (usersNumber % 2 === 0) {
// 		alert('your number is even');
// 	} else {
// 		alert('your number is odd');
// 	}
// } else {
// 	alert('Вы ввели не число');
// }

// Task 3
// Ввод имени , вывести должность, если ввели !имя вывести "Unknown user"
// Mike - CTO, Alex - CEO, Jim - cleaner, Alla - manager
// if/else switch

// const userName = prompt('Enter name');

// if (userName === 'Mike') {
// 	alert('CTO');
// } else if (userName === 'Alex') {
// 	alert('CEO');
// } else if (userName === 'Jim') {
// 	alert('cleaner');
// } else if (userName === 'Alla') {
// 	alert('manager');
// } else {
// 	alert('User not found');
// }

// switch (userName) {
// 	case 'Mike':
// 		alert('CTO');
// 		break;
// 	case 'Alex':
// 		alert('CEO');
// 		break;
// 	case 'Jim':
// 		alert('cleaner');
// 		break;
// 	case 'Alla':
// 		alert('manager');
// 		break;
// 	default:
// 		alert('User not found');
// }

// Task 4
// Пользователь вводит три числа
// Вывести максимальное число и
// Если три числа равны вывести три числа равны
// Если два равны вывести какие два равны
// Если хотя бы одно число не число вывести ошибку
// const userNumber1 = +prompt('Enter number 1');
// const userNumber2 = +prompt('Enter number 2');
// const userNumber3 = +prompt('Enter number 3');
// if (
// 	!Number.isNaN(userNumber1) &&
// 	!Number.isNaN(userNumber2) &&
// 	!Number.isNaN(userNumber3)
// ) {
// 	if (userNumber1 > userNumber2 && userNumber1 > userNumber3) {
// 		alert('Number 1 is max');
// 	} else if (userNumber2 > userNumber1 && userNumber2 > userNumber3) {
// 		alert('Number 2 is max');
// 	} else if (userNumber3 > userNumber1 && userNumber3 > userNumber2) {
// 		alert('Number 3 is max');
// 	}
// 	if (userNumber1 === userNumber2 && userNumber2 === userNumber3) {
// 		alert('Num1 = Num2 = Num3');
// 	} else if (
// 		userNumber1 === userNumber2 ||
// 		userNumber2 === userNumber3 ||
// 		userNumber1 === userNumber3
// 	) {
// 		alert('2 numbers are equal');
// 	}
// } else {
// 	alert('Error');
// }

// Task 5
// напиток и сумма
// чай - 10, кофе - 15, вода - 20
// напиток не существует - error
// сумма invalid - error
// сумму равна стоимости напитка - "Вот ваш ${напиток}"
// сумму больше стоимости напитка - "Вот ваш ${напиток} и сдача ${сдача}"
// сумму меньше стоимости напитка - "Не хватает средств"

const coctel = prompt('u coctel', '');
const price = +prompt('u price', '');
const tea = 10;
const coffee = 15;
const water = 20;

if (coctel === 'tea' || coctel === 'coffee' || coctel === 'water') {
	if (!Number.isNaN(price)) {
		if(tea <= price || tea >= price) { 
			if ( tea === price){ 
				alert(`Вот ваш ${coctel}`)
			} else if ( tea < price) { 
				alert(`Вот ваш ${coctel} и сдача ${price-tea} `)
			} else if ( tea >  price) { 
				alert(`Не хватает ${tea - price}`)
			}			

		}else if (coffee <= price || coffee >= price) {
			if ( coffee === price){ 
				alert(`Вот ваш ${coctel}`)
			} else if ( coffee < price) { 
				alert(`Вот ваш ${coctel} и сдача ${price-coffee} `)
			} else if ( coffee >  price) { 
				alert(`Не хватает ${coffee - price}`)
			}	
		} else if (water <= price || water >= price) { 
			if ( water === price){ 
				alert(`Вот ваш ${coctel}`)
			} else if ( water < price) { 
				alert(`Вот ваш ${coctel} и сдача ${price-water} `)
			} else if ( water >  price) { 
				alert(`Не хватает ${water - price}`)
			}
		}
	} else {
		alert('error prices');
	}
} else {
	alert('error');
}