// const var1 = 'someKey';

// const myObj = {
// 	age: false,
// 	'as as': 'as',
// 	name: 'John',
// 	[var1]: 12,
// };

// myObj[var1] = '1';
// myObj.lastName = '2';

// const key1 = 'name';

// // myObj.age, myObj['age']
// // myObj['as as']
// // myObj[key1]

// //const field = prompt('Enter key'); //key

// // if (field in myObj) {
// // 	alert(myObj[field]);
// // } else {
// // 	alert('Error');
// // }

// // if (myObj[field]) {
// // 	alert(myObj[field]);
// // } else {
// // 	alert('Error');
// // }

// const myObj2 = {
// 	name: 'John',
// 	greetings: function () {
// 		console.log(this);
// 		alert('Hello, my name is' + this.name);
// 	},
// };

// myObj2.greetings();
// console.log(myObj2);

// const myObj3 = {};

// for (let key in myObj2) {
// 	myObj3[key] = myObj2[key];
// }

// console.log(myObj3);
// myObj3.name = 'Liza';

// console.log(myObj2, myObj3);

// // city
// // Kyiv

// let newKey = prompt('Enter a new key');
// while (newKey in myObj3) {
// 	newKey = prompt('Enter a new key');
// }
// const newValue = prompt(`Enter a new value for a key ${newKey}`);
// myObj3[newKey] = newValue;

// console.log(myObj3);

// const keyToDelete = prompt('Enter a key to delete');

// delete myObj3.name;
// delete myObj3[keyToDelete];

// console.log(myObj3);

// 1
// Нужно вывести размеры ноута в консоль используя for..in
// 10 15

const notebook = {
	name: 'Nout',
	size: {
		width: 10,
		height: 15,
		diagonal: 233,
	},
};

for (let key in notebook.size) {
	console.log(notebook.size[key]);
}

// 2
// скопировать объект size из задачи выше (notebook) в новый объект sizeObj
//
const sizeObj = {};
// sizeObj = {
//   width: 10,
//   height: 15,
//   diagonal: 233,
// },

for (let key in notebook.size) {
	sizeObj[key] = notebook.size[key];
}
console.log(sizeObj);

// 3
// Спросить у пользователя имя и хочет ли он кушать
// Проверить валидность имени, переспрашивать пока что-то не так
// Записать значения в объект person
// и добавить туда 2 метода:
// eat - выводить текст "Пойду поем"
// go - выводить текст "Я пошёл, меня зовут ${имя}"
// В зависимости от того хочет ли он кушать вызвать метод либо eat, либо go

let name = prompt('Enter your name');
while (!name.trim()) {
	name = prompt('Enter your name');
}

let question = confirm('Are you hungry?');

let person = {
	name: name,
	answer: question,
	eat: function () {
		alert('Пойду поем');
	},
	go: function () {
		alert(`Я пошел, меня зовут ${this.name}`);
	},
};

if (question) {
	person.eat();
} else {
	person.go();
}

// 4
// Создать объект danItStudent,
// у которого будут такие свойства: имя, фамилия, количество сданных домашних работ.
// Спросить у пользователя "Что вы хотите узнать о студенте?"
// Если пользователь ввел "name" или "имя" - вывести в консоль имя студента.
// Если пользователь ввел "lastName" или "фамилия" - вывести в консольфамилию студента.
// Если пользователь ввел "оценка" или "homeworks" - вывести в консоль количество сданных работ.
// Если ввел, что-то не из перечисленого - вывести в консоль - "Извините таких данных нету"

const danItStudent = {
	name: 'Olha',
	'last name': 'Kyryliuk',
	'number of hw': 10,
};

let answer = prompt(
	'What are you interested in: name (имя), last name (фамилия), number of hw (оценка)?'
);

if (answer === 'имя' || answer === 'name') {
	alert(danItStudent.name);
} else if (answer === 'фамилия' || answer === 'last name') {
	alert(danItStudent['last name']);
} else if (answer === 'оценка' || answer === 'number of hw') {
	alert(danItStudent['number of hw']);
} else {
	alert('Извините таких данных нету');
}

// 5
// создать объект у которого есть один метод который
// добавялет новое поле со значением в текущий объект

function createobj(name) {
	const newObject = {
		title: name,
		add: function (objKey, valueKey) {
			this[objKey] = valueKey;
		},
	};
	return newObject;
}

let answer1 = prompt('Are you sure you want to?');
olyaObject.add('color', 'red');
console.log(olyaObject);
