// var myVariable = 1;

// let, const

let age = 6;
const salary = 100;
console.log('1 ->', age);

let ageOfPerson;
// ! let ageofperson
// ! let age_of_person

age = age + 1;

// salary = 200; error

console.log('textsgsagsagg');
console.log('2 ->', age);

//const x; // error const/let

const myNumber = 1;
const myString = `My age is ${age}`; //My age is 7
const myString1 = 'My age is ' + age; //My age is 7
const myBool = true; // true | false
const myNl = null; // пустота
const myUndef = undefined; //неопреде...
const myArray = ['Olya', 3, true, { state: 'Kyiv' }, 3, 100, 32532];
const myObject = {
	name: 'Oleksandr',
	age: 22,
	work: 'Kyivstar',
};
const myNaN = 2 / 'au'; // NaN
console.log('myNaN', myNaN);
console.log(isNaN(4), isNaN('qwtew')); //false, true

console.log(myObject.age); //22  <--- object  // myObject['age']
console.log(myArray[2]); // true  <--- array

let name1 = 'John';
const admin = name1; // John
console.log(admin); // John

//alert('Hasfgdshfdfkgjdhsgad');
// const answer = confirm('Confirm modal');
// console.log('answer', answer);

// const userAge = prompt('How old are you?');
// console.log('user age', userAge);

console.log(typeof myNumber); // number
console.log(typeof myString); // string
console.log(typeof myBool); // boolean
console.log(typeof myNl); // object
console.log(typeof myUndef); // undefined
console.log(typeof myArray); // object
console.log(typeof myObject); // object
console.log(typeof myNaN); // number
console.log(typeof prompt); // function

console.log(typeof typeof myNumber);

myObject.name = 'Alex'; // not error
