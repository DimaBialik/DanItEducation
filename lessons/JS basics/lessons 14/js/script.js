// // // const title = document.querySelector('p');

// // // title.oncontextmenu = function (e) {
// // //     e.preventDefault();
// // // };

// // const link = document.createElement('a');
// // link.setAttribute('href', 'https://www.google.com');
// // link.innerText = 'Google';

// // link.addEventListener('click', e => {
// //     e.preventDefault();
// //     alert(e.target.innerText);
// // });
// // document.body.append(link);

// // const title = document.querySelector('#title');
// // const linkInTitle = document.querySelector('#title a');

// // linkInTitle.addEventListener('click', e => {
// //     e.preventDefault();
// //     alert('IN LINK', e.target.innerText);
// //     // e.stopPropagation();
// // });
// // title.onclick = function (e) {
// //     alert('IN title', e.target.innerText);
// // };

// // const container = document.querySelector('#cube');
// // let prevTarget = null;
// // container.addEventListener('click', function (e) {
// //     console.log(e.target.id);
// //     if (e.target.id !== 'cube') {
// //         console.log(e.target.innerText);
// //         if (prevTarget) {
// //             prevTarget.classList.remove('pink');
// //             //убрать пинк таргет
// //         }
// //         e.target.classList.add('pink');
// //         prevTarget = e.target;

// //         e.stopPropagation();
// //     }
// // });

// const container = document.querySelector('#cube');
// let prevTarget = null;

// container.addEventListener('click', e => {
//     if (prevTarget) {
//         prevTarget.classList.remove('pink');
//         //убрать пинк таргет
//     }
//     e.target.classList.add('pink');
//     prevTarget = e.target;

//     e.stopPropagation();
// });

// const tree = document.querySelector('#tree');
// tree.addEventListener('click', e => {
//     // console.log(e.target.children);
//     if (e.target.children.length > 0) {
//         const ul = e.target.querySelector('ul');
//         // const ulStyle = window.getComputedStyle(ul).getPropertyValue('display');    ///---------------##1
//         // ul.style.display = ulStyle === 'none' ? 'block' : 'none';

//         // ul.style.display = ul.style.display === 'none' ? 'block' : 'none';    //---------------##2

//         if (ul.style.display === 'none') {
//             ul.style.display = 'block';                                 //---------------#3
//         } else {
//             ul.style.display = 'none';
//         }
//     }
// });

/**
 * Задача 1.
 *
 * Необходимо «оживить» навигационное меню с помощью JavaScript.
 *
 * При клике на элемент меню добавлять к нему CSS-класс .active.
 * Если такой класс уже существует на другом элементе меню, необходимо
 * с того, предыдущего элемента CSS-класс .active снять.
 *
 * У каждый элемент меню — это ссылка, ведущая на google.
 * С помощью JavaScript необходимо предотвратить переход по всем ссылка на этот внешний ресурс.
 *
 * Условия:
 * - В реализации обязательно использовать приём делегирования событий (на весь скрипт слушатель должен быть один).
 */

// const list = document.querySelector('ul');

// list.addEventListener('click', e => {
//     e.preventDefault();
//     const activeLi = list.querySelector('.active');
//     if (activeLi) {
//         activeLi.classList.remove('active');
//     }
//     e.target.classList.add('active');
// });

/**
 * Задача 1.
 
 */
